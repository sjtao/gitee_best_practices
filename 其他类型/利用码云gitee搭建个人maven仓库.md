之前看到使用用了github来做maven仓库的教程，但国内github的速度不给力，个人偏向于使用码云gitee平台，便使用码云搭建了一个maven仓库，记录了下搭建过程。

简单来说，共有三步：

- deploy到本地目录
- 把本地目录提交到码云上
- 配置git地址为仓库地址
### 配置local file maven仓库
#### deploy到本地
maven可以通过http, ftp, ssh等deploy到远程服务器，也可以deploy到本地文件系统里。

例如把项目deploy到C:/gitFiles/maven/repository/目录下：
```
 <distributionManagement>
    <repository>
        <id>xwintop-maven</id>
        <url>file:C:/gitFiles/maven/repository/</url>
    </repository>
</distributionManagement>
```

- 通过命令行则是：
```
mvn deploy -DaltDeploymentRepository=xwintop-maven::default::file:C:gitFiles/maven/repository/
```
推荐使用命令行来deploy，避免在项目里显式配置。

https://maven.apache.org/plugins/maven-deploy-plugin/

https://maven.apache.org/plugins/maven-deploy-plugin/deploy-file-mojo.html

- 把本地仓库提交到github上
上面把项目deploy到本地目录C:/gitFiles/maven/repository/里，下面把这个目录提交到gitee上。

在码云上新建一个项目maven，然后把C:/gitFiles/maven/下的文件都提交到码云上。
```
 cd C:gitFiles/maven
 git init
 git add repository/*
 git commit -m 'deploy xxx'
 git remote add origin git@gitee.com:xwintop/maven.git
 git push origin master
```

最终效果可以参考我的个人仓库：

https://gitee.com/xwintop/maven

gitee码云 maven仓库的使用
因为码云使用了gitee.com/${user_account}/${project_name}/raw/${branch}这个域名用于raw文件下载。所以使用这个maven仓库，只要在pom.xml里增加：
```
 <repositories>
   <repository>
      <id>xwintop-maven</id>
      <url>https://gitee.com/xwintop/maven/raw/master/repository</url>
   </repository>
</repositories>
```

###总结
目前国内使用码云平台托管的项目比较多，速度快，功能强大，推荐大家使用，谢谢。